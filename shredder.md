# Shredder
The shredder need to be low cost and easy to build. There is few interresting projects on Instructable about shredders:

* https://www.instructables.com/id/Low-Cost-Plastic-Shredder/
* https://www.instructables.com/id/Make-a-Mini-Shredder-and-Recycling-3d-Printing-Pla/
* https://www.youtube.com/watch?v=vqWwUx8l_Io

The shredder must be less than 150 € to be affordable.

I thought about a everyday life machine able to shred the plastic like the first or third link and also using a filter to make sure nothing more than 5 mm can pass into the extruder. To make everything already pre cut, maybe using a paper shreder beofer putting into the machine.